# timer.rb

class Timer
  attr_accessor :seconds

  def initialize
    @seconds = 0
  end

  def time_string
    hours = @seconds / 3600
    minutes = (@seconds - hours * 3600) / 60
    seconds = @seconds - (minutes * 60 + hours * 3600)

    "#{padded(hours)}:#{padded(minutes)}:#{padded(seconds)}"
  end

  def padded(num)
    if num < 10 && num > -1
      "0#{num}"
    else
      num.to_s
    end
  end
end
