class Friend
  def greeting(who = "")
    if who.empty?
      "Hello!"
    else
      "Hello, #{who}!"
    end
  end
end
