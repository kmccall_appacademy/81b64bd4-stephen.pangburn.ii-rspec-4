class Temperature
  def initialize(opts = { f: nil, c: nil })
    @fahrenheit = opts[:f]
    @celsius = opts[:c]
  end

  def self.from_celsius(temp)
    Temperature.new(:c => temp)
  end

  def self.from_fahrenheit(temp)
    Temperature.new(:f => temp)
  end

  def in_fahrenheit
    if !@fahrenheit.nil?
      @fahrenheit
    else
      @fahrenheit = ctof(@celsius)
    end
  end

  def in_celsius
    if !@celsius.nil?
      @celsius
    else
      @celsius = ftoc(@fahrenheit)
    end
  end

  def ftoc(f)
    (f - 32) * (5.0 / 9.0)
  end

  def ctof(c)
    c * (9.0 / 5.0) + 32
  end
end

class Celsius < Temperature
  def initialize(c)
    @celsius = c
  end
end

class Fahrenheit < Temperature
  def initialize(f)
    @fahrenheit = f
  end
end
