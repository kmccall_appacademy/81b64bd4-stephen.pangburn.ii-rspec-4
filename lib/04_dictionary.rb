class Dictionary
  attr_accessor :entries

  def initialize
    @entries = {}
  end

  def add(pair)
    if pair.is_a?(String)
      @entries[pair] = nil
    else
      @entries.merge!(pair)
    end
  end

  def keywords
    @entries.keys.sort
  end

  def include?(key)
    @entries.include?(key)
  end

  def find(key)
    hash = Hash.new
    items = @entries.keys.select { |k| k.start_with?(key) }

    items.each do |item|
      hash[item] = @entries[item]
    end
    hash
  end

  def printable
    print_string = ""

    @entries.each do |k, v|
      print_string << "[#{k}] \"#{v}\"\n"
    end
    print_string.split("\n").sort.join("\n")
  end
end
