class Book
  def title=(string)
    exceptions = "and in the of a an to".split(" ")

    @title = string.capitalize.split.map do |word|
      exceptions.include?(word) ? word : word.capitalize
    end.join(" ")
  end

  def title
    @title
  end
end
