# # Hello, friend!
#
# This lab teaches basic Ruby Object syntax.
#
# ## Watch it fail
#
# Your first real failure should be something like this:
#
#     ./friend_spec.rb:3: uninitialized constant Friend (NameError)
#
# Fix this by opening `friend.rb` and creating an empty class:
#
#     class Friend
#     end
#
# Save it. Run the test again.
#
# ## Watch it fail again
#
# ...
#
# (...Yeah, I got it :| )

require "00_hello_friend"

describe Friend do
  it "says hello" do
    expect(Friend.new.greeting).to eq("Hello!")
  end

  it "says hello to someone" do
    expect(Friend.new.greeting("Bob")).to eq("Hello, Bob!")
  end
end
